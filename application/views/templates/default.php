<?php
$this->load->view('templates/header'); //This corresponds to /application/views/templates/header.php
$this->load->view($content); //This depends on the $data['content'] value passed by the corresponding controller function
$this->load->view('templates/footer'); //This corresponds to /application/views/templates/footer.php
?>
